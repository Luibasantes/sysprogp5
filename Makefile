# Este es el archivo Makefile de la práctica, editarlo como sea neceario

IDIR = ../include
CC = gcc
CFLAGS = -I$(IDIR)

ODIR=obj
LDIR = ../lib

LIBS = -lm

_DEPS = vector.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = main.o vector.o 
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR)/%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

.PHONY: clean

# Target que se ejecutará por defecto con el comando make
all: dynamic

# Target que compilará el proyecto usando librerías estáticas
static: $(OBJ)
	gcc -c $(LDIR)/vector.c
	ar rcs libvector.a $(LDIR)/vector.o
	gcc -static -o $@ main.c ./libvector.a $(CFLAGS) $(LIBS)

# Target que compilará el proyecto usando librerías dinámicas
dynamic: $(OBJ)
	gcc -shared -fPIC -o libvector.so $(LDIR)/vector.c
	gcc -o $@ main.c ./libvector.so $(CFLAGS) $(LIBS)

# Limpiar archivos temporales
clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ 
