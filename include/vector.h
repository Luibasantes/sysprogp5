#ifndef VECTOR_H
#define VECTOR_H

/* Definicion del tipo de datos vector3D */
typedef struct {
	float x;
	float y;
	float z;
} vector3D;

// Función que calcule el producto escalar entre dos vectores
float dotproduct(vector3D v1, vector3D v2);

// Función que calcule el producto vectorial entre dos vectores
vector3D crossproduct(vector3D v1, vector3D v2);

// Función que calcule la magnitud de un vector
float magnitud(vector3D v);

// Función que determine si dos vectores son ortogonales (retornar 0 si no lo son)
int esOrtogonal(vector3D v1, vector3D v2);

#endif
